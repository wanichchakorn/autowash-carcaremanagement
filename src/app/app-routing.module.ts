import { Routes } from '@angular/router';
import { BlankComponent } from './layouts/blank/blank.component';
import { FullscreenComponent } from './layouts/fullscreen/fullscreen.component';
import { AuthGuard } from './_helpers';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NotfoundsComponent } from './authen/404/not-found.component';

export const Approutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: '',
    component: BlankComponent,
    children: [
    
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },

    ]
  },
  {
    path: '',
    component: FullscreenComponent,
    children: [
     
      {
        canActivate: [AuthGuard],
        path: 'staff',
        loadChildren: () => import('./staff/staff.module').then(m => m.StaffModule)
      },

    ]
  },
  {
    path: '**', 
    component: NotfoundsComponent
  }

];

