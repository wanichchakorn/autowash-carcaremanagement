import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormatDateService {

  m: number;
  month: string;
  y: number;
  year: number;
  day: number;
  date: any;

  constructor() { }

  todayDate: Date = new Date();

  config() {

    this.day = this.todayDate.getDate()
    this.m = this.todayDate.getMonth();
    this.year = this.todayDate.getFullYear() + 543;

    if (this.m == 0) {
      this.month = 'มกราคม';
    } else if (this.m == 1) {
      this.month = 'กุมภาพันธ์';
    } else if (this.m == 2) {
      this.month = 'มีนาคม';
    } else if (this.m == 3) {
      this.month = 'เมษายน';
    } else if (this.m == 4) {
      this.month = 'พฤษภาคม';
    } else if (this.m == 5) {
      this.month = 'มิถุนายน';
    } else if (this.m == 6) {
      this.month = 'กรกฎาคม';
    } else if (this.m == 7) {
      this.month = 'สิงหาคม';
    } else if (this.m == 8) {
      this.month = 'กันยายน';
    } else if (this.m == 9) {
      this.month = 'ตุลาคม';
    } else if (this.m == 10) {
      this.month = 'พฤศจิกายน';
    } else if (this.m == 11) {
      this.month = 'ธันวาคม';
    }

    // console.log(this.day)
    // console.log(this.month)
    // console.log(this.year)
    return this.date = {'d':this.day,'m':this.month,'y':this.year};
    // console.log(this.date)
  }


}
