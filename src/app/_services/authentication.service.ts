import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { AlertService } from 'src/app/_services/alert.service';
import { Router } from '@angular/router';
import { Role } from '../_models/';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  inBranch: any;
  decode() {
    throw new Error("Method not implemented.");
  }
  loading = false;
  submitted = false;
  returnUrl: string;
  cookieValue: string;
  role: string;

  constructor(
    private cookieService: CookieService,
    private alertService: AlertService,
    private router: Router
  ) { }

  onLogin(phoneNumber: string, password: string): any {
    // console.log("onLogin")
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("authorization", "Basic VnpOYWEyUjVlSEZPVmpWcg==");
    var inUserID = { mobile_no: phoneNumber, password: password };

    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(inUserID),
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/basic/credential/login`, requestOptions).then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        this.inBranch = res.data.user
        let imgBranch = this.inBranch.branch
        // console.log(res)
        this.cookieService.set('mobile_no', res.data.user.mobile_no);
        this.cookieService.set('first_name', res.data.user.first_name);
        this.cookieService.set('last_name', res.data.user.last_name);
        this.cookieService.set('date_of_birth', res.data.user.date_of_birth)
        this.cookieService.set('authorization', "Bearer " + res.data.token);
        this.cookieService.set('role_id', res.data.user.role_id);
        this.cookieService.set('branch_name', this.inBranch.branch.branch_name);
        this.cookieService.set('image_path', imgBranch.branch_qr.image_path);
        this.cookieValue = this.cookieService.get('authorization');
        this.role = this.cookieService.get('role_id');
      })
      .then(res => {
        if (this.cookieService.get('role_id') == "ROLE_000") {
          this.router.navigate(['/staff/queued']);
          this.cookieService.set('branch_id', this.inBranch.branch_id);
        }else{
          this.cookieService.set('branch_id', null);
          this.router.navigate(['/staff/home']);
        }
      },
        error => {
          this.alertService.error(error);
          this.loading = false;
        })
  }

  isRole() {
    return this.cookieService.get('authorization') && this.cookieService.get('role_id');
  }

  logout() {
    // console.log('logout success')
    // remove user from local storage to log user out
    return new Promise((resolve) => {
      this.cookieService.delete('mobile_no');
      this.cookieService.delete('first_name');
      this.cookieService.delete('branch_id');
      this.cookieService.delete('authorization');
      this.cookieService.delete('role_id');
      localStorage.clear()
      resolve(true);
      // console.log(this.cookieService.get('authorization'))
    });
  }


}
