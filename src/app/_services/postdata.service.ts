import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HeaderService } from '../_services/header.service';

@Injectable({
  providedIn: 'root'
})
export class PostdataService {

  car_ID: any = [];
  service_ID: any = [];
  promotion_ID: string;
  imgCars: string = null;
  imgName: string = null;
  newDate = new Date();

  constructor(private header: HeaderService) { }

  onPostCarCustomer(carCustomers, service) {
    // console.log(carCustomers,service)
    const myHeaders = this.header.onPost();
    const branch = this.header.onBranch();
    var raw = JSON.stringify({ car_id: carCustomers, services_id: service, promotion_id: "", wait: false, wait_time: "", branch_id: branch });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/auth/job/create`, requestOptions)
      .then(response => response.text())
      .then(result => { return JSON.parse(result) })
      .catch(error => console.log('error', error));
  }

  // console.log(data, res)
  // const imgCar = data.inImgCar.split("\\");
  // this.imgCars = imgCar[imgCar.length - 1]
  // console.log(this.imgCars);

  onUploadImg(img) {
    // console.log(img)
    this.imgName = img.name
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic VnpOYWEyUjVlSEZPVmpWcg==");
    var formdata = new FormData();
    formdata.append("image", img, this.imgName);
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: formdata,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/basic/upload/image`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res.data;
      })
      .catch(error => console.log('error', error));
  }

  onPostNewCustomer(dataCarCustomer, newCustomer, img, dataBrand, dataColor, phone) {
    // console.log(dataCarCustomer, newCustomer, img, dataBrand, dataColor, phone)
    const myHeaders = this.header.onPost();
    const branch = this.header.onBranch();
    var raw = JSON.stringify({ "car": { "ID": "", "plate_number": dataCarCustomer.inLicense, "brand_id": dataBrand.id, "brand_name": dataBrand.brand_name, "model": dataCarCustomer.inModel, "color_obj": { "id": dataColor.id, "title": dataColor.title, "hex": "", "rgb": { "red": 0, "green": 0, "blue": 0 } }, "title": "", "hex": "", "car_photo_id": img, "created_date_time": this.newDate, "branch_id": branch, "customer": { "ID": "", "created_date_time": this.newDate }, "store": false }, "customer": { "ID": "", "created_date_time": this.newDate, "customer_name": newCustomer.inNewCustomer, "phones": phone } });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/car/add_car`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  onPostNewCarCustomer(dataCarCustomer, customerID, customerName, img, dataBrand, dataColor) {
    // console.log(dataCarCustomer, customer, img, dataBrand, dataColor)
    const myHeaders = this.header.onPost();
    const branch = this.header.onBranch();
    var raw = JSON.stringify({ "customer_id": customerID, "CustomerName": customerName, "car_id": "", "car": { "plate_number": dataCarCustomer.inLicense, "brand_id": dataBrand.id, "model": dataCarCustomer.inModel, "color": dataColor.id, "car_photo_id": img, "branch_id": branch } });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/auth/car/add_car`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  onPostUpdateCustomer(custId, cust, img) {
    // console.log(custId, cust.inNameCustomer,cust.inNumberPhone, img)
    const myHeaders = this.header.onPost();
    var raw = JSON.stringify({ "customer_id": custId, "customer": { "customer_name": cust.inNameCustomer, "phones": [{ "phone": cust.inNumberPhone }], "profile_image_id": img } });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(`${environment.API_URL}/api/v1/auth/car/update_user_profile`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  onPostNewRegister(dataRegister, date) {
    // console.log(dataRegister, date);
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic VnpOYWEyUjVlSEZPVmpWcg==");
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({ "mobile_no": dataRegister.phoneNumber, "first_name": dataRegister.firstName, "last_name": dataRegister.lastName, "password": dataRegister.password, "date_of_birth": date, "gender": dataRegister.sex, "role_id": "ROLE_000" });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/basic/credential/register`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  // onPostOTP(dataRegister) {
  //   var myHeaders = new Headers();
  //   myHeaders.append("Content-Type", "application/json");
  //   myHeaders.append("Authorization", "Basic VnpOYWEyUjVlSEZPVmpWcg==");
  //   var raw = JSON.stringify({ "mobile_no": dataRegister.phoneNumber });
  //   var requestOptions: RequestInit = {
  //     method: 'POST',
  //     headers: myHeaders,
  //     body: raw,
  //     redirect: 'follow'
  //   };
  //   return fetch(`${environment.API_URL}/api/v1/basic/credential/request_otp`, requestOptions)
  //     .then(response => response.text())
  //     .then(result => {
  //       let res = JSON.parse(result)
  //       // console.log(res)
  //       return res;
  //     })
  //     .catch(error => console.log('error', error));
  // }

  onPostIdOTP(code,otp) {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "Basic VnpOYWEyUjVlSEZPVmpWcg==");
    var raw = JSON.stringify({ "ref_code": code, "otp": otp });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/basic/credential/verify_otp`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

}
