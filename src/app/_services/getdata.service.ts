import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HeaderService } from './header.service';

@Injectable({
  providedIn: 'root'
})
export class GetdataService {

  service: any = [];
  carCustomer: any = [];
  carCustomers: any = [];

  constructor(private header: HeaderService) { }

  onGetCustomer() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/auth/customer/customers?branchid=${branch}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        for (var i = 0; i < res.data.length; i++) {
          for (var i2 = 0; i2 < res.data[i].cars.length; i2++) {
            this.carCustomer.push({
              ID: res.data[i].cars[i2].ID,
              car: `${res.data[i].cars[i2].brand_name} ${res.data[i].cars[i2].model} , ทะเบียน : ${res.data[i].cars[i2].plate_number}`,
            })
          }
        }
        return this.carCustomers = this.carCustomer
      })
      .catch(error => console.log('error', error));
  }

  onGetService() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/auth/service/services?branchId=${branch}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        if (res == "") {
          return null;
        }
        return res.data;
      })
      .catch(error => console.log('error', error));
  }

  onGetJob() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    return fetch(`${environment.API_URL}/api/v1/auth/job/jobs?orderby=desc&branchId=${branch}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  onGetCustomers() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    return fetch(`${environment.API_URL}/api/v1/auth/customer/customers?branchid=${branch}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  onGetFollowUp() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    return fetch(`${environment.API_URL}/api/v1/auth/car/followup?branchid=${branch}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  onGetCarCustomer() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    return fetch(`${environment.API_URL}/api/v1/auth/car/cars?branchid=${branch}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  onGetBrands() {
    const myHeaders = this.header.onGet();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    return fetch(`${environment.API_URL}/api/v1/auth/brand_model/brands`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res.data;
      })
      .catch(error => console.log('error', error));
  }

  onGetColors() {
    const myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic VnpOYWEyUjVlSEZPVmpWcg==");
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    return fetch(`${environment.API_URL}/api/v1/basic/color/colors`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res.data;
      })
      .catch(error => console.log('error', error));
  }

  onGetCustomerProfile(custId) {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    return fetch(`${environment.API_URL}/api/v1/auth/customer/customers?branchid=${branch}&id=${custId}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

}
