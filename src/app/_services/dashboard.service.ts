import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HeaderService } from './header.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private header: HeaderService) { }

  onGetCount(startDate, endDate) {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    // console.log(branch);
    return fetch(`${environment.API_URL}/api/v1/auth/dashboard/services/usage?branchid=${branch}&startDate=${startDate}&endDate=${endDate}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  onGetTransactionCount(startDate, endDate) {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    // console.log(branch);
    return fetch(`${environment.API_URL}/api/v1/auth/dashboard/transactions/counts?branchid=${branch}&startDate=${startDate}&endDate=${endDate}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        //fix API send 2 JSON
        // let res = JSON.parse(result.split("\"}")[1])
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  onGetTransactionPayment(startDate, endDate) {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/auth/dashboard/transactions/payments?branchid=${branch}&startDate=${startDate}&endDate=${endDate}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        return res;
      })
      .catch(error => console.log('error', error));
  }

  onGetCarCondition() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/car/cars?branchid=${branch}`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  onGetCarFollowUp() {
    const myHeaders = this.header.onGet();
    const branch = this.header.onBranch();
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/car/followup?branchid=${branch}`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }
 
}


