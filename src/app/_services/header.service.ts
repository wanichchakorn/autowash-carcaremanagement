import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(private cookieService: CookieService) { }

  onGet() {
    var myHeaders = new Headers();
    const authen: string = this.cookieService.get('authorization');
    myHeaders.append('authorization', authen);
    // console.log(authen);
    return myHeaders;
  }

  onPost() {
    var myHeaders = new Headers();
    const authen: string = this.cookieService.get('authorization');
    myHeaders.append('authorization', authen);
    myHeaders.append("Content-Type", "application/json");
    return myHeaders;
  }

  onBranch() {
    const branch_id: string = this.cookieService.get('branch_id');
    return branch_id;
  }

}
