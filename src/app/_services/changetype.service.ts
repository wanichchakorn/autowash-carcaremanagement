import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HeaderService } from './header.service';

@Injectable({
  providedIn: 'root'
})
export class ChangetypeService {
  price: string;
  ID: string;
  confirm: boolean;
  carServices: any;

  constructor(private header: HeaderService) { }

  onGetPay(ID) {
    this.ID = ID;
  }

  onGetDelete(ID) {
    this.ID = ID;
  }

  onDeleteConfirm(confirm) {
    this.confirm = confirm
    if (this.confirm == true) {
      this.onDeleteJob(this.ID)
    }
  }

  onGetPrice(price) {
    this.price = price.price.toString();
    // console.log(this.price )
    this.onPayJob(this.ID, this.price)
  }

  onStartJob(ID) {
    const myHeaders = this.header.onPost();
    var raw = JSON.stringify({ "job_id": ID });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/job/start_job`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  onDoneJob(ID) {
    const myHeaders = this.header.onPost();
    var raw = JSON.stringify({ "job_id": ID });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/job/done_job`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  onPayJob(ID, price) {
    // console.log(ID, price)
    const myHeaders = this.header.onPost();
    var raw = JSON.stringify({ "job_id": ID, "price": price });
    // console.log(raw)
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    return fetch(`${environment.API_URL}/api/v1/auth/job/paid_job`, requestOptions)
      .then(response => response.text())
      // .then(result => {
      //   let res = JSON.parse(result)
      //   console.log(res)
      //   return res.data;
      // })
      .catch(error => console.log('error', error));
  }

  onDeleteJob(ID) {
    const myHeaders = this.header.onPost();
    var raw = JSON.stringify({ "job_id": ID });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/job/delete`, requestOptions)
      .then(response => response.text())
      // .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }


}
