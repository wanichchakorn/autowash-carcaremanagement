import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AuthenticationService } from './_services';
import { Role } from './_models/';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';

  constructor( private cookieService: CookieService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}
  ngOnInit(): void {
  }

  get isRole() {
    return this.cookieService.get('authorization') && this.cookieService.get('role_id') === Role.ROLE_001;
  }
  
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
}
  
}
