import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { PostdataService } from '../_services/postdata.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  newDate = new Date();
  format = 'MM-dd';
  formatYear = 'yyyy-MM-dd';
  formatBrith = 'dd-MM-yyyy';
  locale = 'en-US';
  onDate: string;
  registrationForm: FormGroup;
  addOTPForm: FormGroup;
  showSuccess = false;
  showWarning = false;
  showDanger = false;
  showPassword = false;
  otpCode: any;

  constructor(
    public postdataService: PostdataService,
    private formBuilder: FormBuilder,
    private router: Router,
    private _modalService: NgbModal
  ) { }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      firstName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      lastName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      dateOfBrith: [null, [Validators.required]],
      sex: [null, [Validators.required]],
      phoneNumber: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(12)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      confirmPassword: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(12)]]
    });

    this.addOTPForm = this.formBuilder.group({
      otp: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]
    })

    let date = formatDate(this.newDate, this.format, this.locale)
    let year = 543 + this.newDate.getFullYear()
    let toDay = year + '-' + date
    this.onDate = formatDate(toDay, this.formatYear, this.locale)
  }

  onSubmitRegistrantion(inaddOTP) {
    let dataRegister = this.registrationForm.value
    if (this.registrationForm.invalid) {
      this.showWarning = true;
      this.showDanger = false;
      this.showPassword = false;
      this.registrationForm.reset();
    } else {
      if(dataRegister.password !== dataRegister.confirmPassword) {
        this.showWarning = false;
        this.showDanger = false;
        this.showPassword = true;
        this.registrationForm.reset();
      }else{
        let toDay = this.registrationForm.get('dateOfBrith').value
      let date = formatDate(toDay, this.formatBrith, this.locale)
      // console.log(date);
      this.postdataService.onPostNewRegister(this.registrationForm.value, date)
        .then(res => {
          // console.log(res)
          if (res.description == "Duplicate registration, this mobile number already used") {
            // console.log('this mobile number already used');
            this.showWarning = false;
            this.showDanger = true;
            this.showPassword = false;
            this.registrationForm.reset();
          } else {
            this._modalService.open(inaddOTP)
            let dataOTP = res.data.otp
            this.otpCode = dataOTP.ref_code
            // this.onSendOTP(dataRegister)
            this.showSuccess = true;
            this.showWarning = false;
            this.showDanger = false;
            this.showPassword = false;
            this.registrationForm.reset();
          }
        })
      }
    }
  }

  onSendOTP(dataRegister) {
    // this.postdataService.onPostOTP(dataRegister)
    // .then(res => {
    //   this.otpCode = res.data.ref_code
    // })
  }

  onSubmitAddOTP(inSuccess) {
    let otpData = this.addOTPForm.value
    if(this.addOTPForm.invalid){ 
      this.postdataService.onPostIdOTP(this.otpCode,otpData.otp)
      .then(res => {
        console.log(res)
        if (res.success == "false") {
          this.showSuccess = false;
          this.showWarning = false;
          this.showDanger = true;
          this.addOTPForm.reset();
        } else {
          this._modalService.open(inSuccess)
        }
      })
    }
  }

  goToLogin() {
    this.router.navigateByUrl('/login');
  }

  goToSuccess() {
    this.router.navigateByUrl('/login');
  }

}
