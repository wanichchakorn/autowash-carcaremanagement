import { Component, OnInit } from '@angular/core';
import * as tableData from './getservicedata';
import { LocalDataSource } from 'ng2-smart-table';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';

export interface PeriodicElement {
  ID: string;
  service_name: string;
  price: number;
}

var ELEMENT_DATA: PeriodicElement[] = [];

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  source: LocalDataSource;
  myHeaders: Headers;
  branch_id: any;

  constructor(
    private cookieService: CookieService
  ) {
    // this.source = new LocalDataSource(tableData.data); // create the source
    this.source = new LocalDataSource(ELEMENT_DATA);
  }
  settings = tableData.settings;

  ngOnInit() {
    this.onGetService();
  }

  onGetService() {
    this.source = new LocalDataSource(ELEMENT_DATA);
    this.myHeaders = new Headers();
    const authen: string = this.cookieService.get('authorization');
    this.branch_id = this.cookieService.get('branch_id');
    // console.log(authen)
    // console.log(branch_id)
    this.myHeaders.append('authorization', authen);
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: this.myHeaders,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/service/services?branchId=${this.branch_id}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        for (var i = 0; i < res.data.length; i++) {
          ELEMENT_DATA.push({
            ID: res.data[i].ID,
            service_name: res.data[i].service_name,
            price: res.data[i].price
          })
        }
        this.source = new LocalDataSource(ELEMENT_DATA);
      })
      .catch(error => console.log('error', error));
  }

  onCreateConfirm($event) {
    // console.log($event)
    // console.log("Create Event In Console")
    let price = 0;
    price = parseInt($event.newData.price)
    var raw = JSON.stringify({ service_name: $event.newData.service_name, price: price, branch_id: this.branch_id });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: this.myHeaders,
      body: raw,
      redirect: 'follow'
    };
    // console.log(requestOptions)
    fetch(`${environment.API_URL}/api/v1/auth/service/create`, requestOptions)
      .then(response => response.text())
      .then(result => {
        // console.log(result)
        $event.confirm.resolve();
      })
      .catch(error => console.log('error', error));
  }

  onSaveConfirm($event) {
    // console.log("Edit Event In Console")
    // console.log($event);
    let price = 0;
    price = parseInt($event.newData.price)
    var raw = JSON.stringify({service_name: $event.newData.service_name, price: price, branch_id: this.branch_id });
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: this.myHeaders,
      body: raw,
      redirect: 'follow'
    };
    // console.log(requestOptions)
    fetch(`${environment.API_URL}/api/v1/auth/service/update/${$event.newData.ID}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        console.log(result)
        $event.confirm.resolve();
      })
      .catch(error => console.log('error', error));
  }

  onDeleteConfirm($event) {
    console.log("Delete Event In Console")
    // console.log($event);
    var raw = JSON.stringify({id: $event.data.ID});
    var requestOptions: RequestInit = {
      method: 'POST',
      headers: this.myHeaders,
      body: raw,
      redirect: 'follow'
    };
    if (window.confirm('คุณต้องการลบข้อมูลนี้หรือไม่ ?')) {
      console.log(requestOptions)
      fetch(`${environment.API_URL}/api/v1/auth/service/delete`, requestOptions)
        .then(response => response.text())
        .then(result => {
          console.log(result)
          $event.confirm.resolve();
        })
        .catch(error => console.log('error', error));
    } else {
      $event.confirm.reject();
    }
  }

  displayedColumns: string[] = [
    // 'ID',
    'service_name',
    'price'
  ];

}
