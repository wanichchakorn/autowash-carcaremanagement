export let settings = {
  
  columns: {

      service_name: {
        title: 'ชื่อบริการ',
        filter: true,
        inputClass: ''
      },
      price: {
        title: 'ราคา',
        filter: true,
        inputClass: ''
      }
    },
    actions: {
      columnTitle: 'การจัดการ',
      add: true,
      edit: true,
      delete: true,
      custom: [],
      position: 'left', // left|right
    },
    attr: {
      id: '',
      class: '',
    },
    noDataMessage: 'ไม่พบข้อมูล',
    add:{
      addButtonContent: 'เพิ่มบริการ',
      createButtonContent: '<i class="ti-save text-success m-r-10"></i>',
      cancelButtonContent: '<i class="ti-close text-danger"></i>',
      confirmCreate: true
    },
    edit: {
      editButtonContent: '<i class="ti-pencil text-info m-r-10"></i>',
      saveButtonContent: '<i class="ti-save text-success m-r-10"></i>',
      cancelButtonContent: '<i class="ti-close text-danger"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="ti-trash text-danger m-r-10"></i>',
      saveButtonContent: '<i class="ti-save text-success m-r-10"></i>',
      cancelButtonContent: '<i class="ti-close text-danger"></i>',
      confirmDelete: true
    }
  };
  export let data = [
    {
      ID: 10002,
      service_name: 'ล้างรถ',
      price: '200'
    },
    {
      ID: 20002,
      service_name: 'ดูดฝุ่น',
      price: '150'
    }
  ];