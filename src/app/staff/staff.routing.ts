import { Routes } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { AuthGuard } from '../_helpers';
import { ChangeBranchComponent } from "./change-branch/change-branch.component";
import { QueuedCustomerComponent } from "./queued-customer/queued-customer.component";
import { AddJobComponent } from './add-job/add-job.component';
import { ServiceComponent } from './service/service.component';
import { FollowUpComponent } from './followup/followup.component';
import { CarCustomerComponent } from './car-customer/car-customer.component';
import { AddCarCustomerComponent } from './add-car-customer/add-car-customer.component';
import { PromotionComponent } from './promotion/promotion.component';
import { AboutAutowashComponent } from './about-autowash/about-autowash.component';
import { DownloadAutowashComponent } from './download-autowash/download-autowash.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CarInStoreComponent } from './car-in-store/car-in-store.component';
import { CustomerComponent } from './customer/customer.component';
import { Role } from '../_models';

export const StaffRoutes: Routes = [
  {
    path: '',
    children: [
      {
        canActivate: [AuthGuard],
        path: 'home',
        component: HomeComponent,
        data: {
          title: 'Home'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'branch',
        component: ChangeBranchComponent,
        data: {
          role: Role.ROLE_001,
          title: 'Branch'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'queued',
        component: QueuedCustomerComponent,
        data: {
          data: Role,
          title: 'Queued'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'addjob',
        component: AddJobComponent,
        data: {
          role: Role,
          title: 'AddJob'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'service',
        component: ServiceComponent,
        data: {
          role: Role.ROLE_001,
          title: 'Service'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'followup',
        component: FollowUpComponent,
        data: {
          role: Role.ROLE_001,
          title: 'Follow UP'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'carcustomer',
        component: CarCustomerComponent,
        data: {
          role: Role,
          title: 'CarCustomer'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'addcarcustomer',
        component: AddCarCustomerComponent,
        data: {
          role: Role,
          title: 'AddCarCustomer'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'customer',
        component: CustomerComponent,
        data: {
          role: Role,
          title: 'Customer'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'carinstore',
        component: CarInStoreComponent,
        data: {
          role:  Role,
          title: 'Carinstore'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'aboutautowash',
        component: AboutAutowashComponent,
        data: {
          role: Role,
          title: 'Aboutautowash'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'downloadsutowash',
        component: DownloadAutowashComponent,
        data: {
          role: Role,
          title: 'Downloadsutowash'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'profile',
        component: ProfileComponent,
        data: {
          role: Role,
          title: 'Profile'
        }
      },
      {
        canActivate: [AuthGuard],
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          role: Role.ROLE_001,
          title: 'Dashboard'
        }
      }
    ]
  }
];
