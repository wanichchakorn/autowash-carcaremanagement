import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { GetdataService } from 'src/app/_services/getdata.service';
import { PostdataService } from 'src/app/_services/postdata.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-car-customer',
  templateUrl: './add-car-customer.component.html',
  styleUrls: ['./add-car-customer.component.css']
})
export class AddCarCustomerComponent implements OnInit {

  addCarCustomerForm: FormGroup;
  addNewCustomerForm: FormGroup;
  addNewCarOldCustomerForm: FormGroup;

  dropdownBrand: any = {};
  dropdownColor: any = {};
  dropdownCustomer: any = {};
  closeDropdownSelection = false;
  disabled = false;

  brands: any = [];
  colors: any = [];
  customers: any = [];
  imgCar: any = null;
  dataImg: any = null;
  dataCarCustomer: any;
  brand_ID: string;
  color_ID: string;
  customer_ID: string;
  customers_name: string;
  dataBrand: { id: string; brand_name: string; };
  dataColor: any = []; 

  submitted = false;
  showSuccess = false;
  showWarning = false;
  showDanger = false;
  loading = false;
  message: string = null;
  imgURL: any  = null;
  imagePath: any = null;

  constructor(
    private formBuilder: FormBuilder,
    private getDataService: GetdataService,
    private postDataService: PostdataService,
    private _modalService: NgbModal
  ) { }

  async ngOnInit() {

    this.addCarCustomerForm = this.formBuilder.group({
      inImg: [null],
      inLicense: [null, [Validators.required]],
      inBrand: [null, [Validators.required]],
      inModel: [null],
      inColor: [null]
    });

    this.addNewCustomerForm = this.formBuilder.group({
      inNewCustomer: [null, [Validators.required]],
      inNumberPhone: this.formBuilder.array([this.formBuilder.group({ phone: [null, [Validators.required]] })])
    });

    this.addNewCarOldCustomerForm = this.formBuilder.group({
      inCustomer: [null, [Validators.required]]
    });

    this.dropdownBrand = {
      singleSelection: true,
      searchPlaceholderText: 'ค้นหา',
      noDataAvailablePlaceholderText: 'ไม่พบข้อมูล',
      allowSearchFilter: true,
      idField: 'id',
      textField: 'brand_name',
      closeDropDownOnSelection: this.closeDropdownSelection
    };

    this.dropdownColor = {
      singleSelection: true,
      searchPlaceholderText: 'ค้นหา',
      noDataAvailablePlaceholderText: 'ไม่พบข้อมูล',
      allowSearchFilter: true,
      idField: 'id',
      textField: 'title',
      closeDropDownOnSelection: this.closeDropdownSelection
    };

    this.dropdownCustomer = {
      singleSelection: true,
      searchPlaceholderText: 'ค้นหา',
      noDataAvailablePlaceholderText: 'ไม่พบข้อมูล',
      allowSearchFilter: true,
      idField: 'ID',
      textField: 'dataCustomer',
      closeDropDownOnSelection: this.closeDropdownSelection
    };

    this.brands = await this.getDataService.onGetBrands()
    this.colors = await this.getDataService.onGetColors()
    await this.onGetCustomer();

    this.imgURL = "../assets/images/upload/upload.png"
  }
  get f() { return this.addCarCustomerForm.controls; }

  get numberPhone() {
    return this.addNewCustomerForm.get('inNumberPhone') as FormArray;
  }

  onAddNumberPhone() {
    this.numberPhone.push(this.formBuilder.group({ phone: '' }));
  }

  imageCar(fileInput) {
    this.imgCar = fileInput.files[0]
    // console.log(fileInput.files[0]);
    if (this.imgCar == null) {
      this.message = "อัพโหลดรูป";
      return;
    }else{
      this.message = this.imgCar.name
    }
    var reader = new FileReader();
    this.imagePath = fileInput.files[0];
    reader.readAsDataURL(this.imgCar); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

  onSubmitNewCar(data, inSelectCustomer) {
    this.submitted = true;
    // console.log(data)
    this.onCheckImg()
    if (this.addCarCustomerForm.invalid) {
      this.showSuccess = false;
      this.showWarning = true;
      this.showDanger = false;
    } else {
      this.loading = true;
      this._modalService.open(inSelectCustomer)
      this.dataCarCustomer = data;
      var brand = this.dataCarCustomer.inBrand;
      var color = this.dataCarCustomer.inColor;
      // console.log(brand[0].id);
      const brandId = brand[0].id;
      const brandName = brand[0].brand_name;
      this.dataBrand = ({
        id: brandId,
        brand_name: brandName
      })
      if (color == null) {
      } else {
        const colorId = color[0].id;
        const colorTitle = color[0].title;
        this.dataColor = ({
          id: colorId,
          title: colorTitle
        })
      }
    }
  }

  async onCheckImg() {
    if (this.imgCar == null) {
      this.imgCar = null;
    } else {
      await this.postDataService.onUploadImg(this.imgCar)
        .then(res => { this.dataImg = res })
      // .then(res => {console.log(res)})
    }
  }

  onSubmitAddNewCustomer(data) {
    console.log(data);
    const phone = data.inNumberPhone;
    if (this.addCarCustomerForm.invalid) {
      this.showSuccess = false;
      this.showWarning = true;
      this.showDanger = false;
    } else {
      this.loading = true;
      if (this.dataImg == null) {
        this.dataImg = "../assets/images/upload/carCust.jpg"
        this.postDataService.onPostNewCustomer(this.dataCarCustomer, data, this.dataImg, this.dataBrand, this.dataColor, phone)
        this.addCarCustomerForm.reset();
        this.showSuccess = true;
        this.showWarning = false;
        this.onSuccessForm()
      } else {
        this.postDataService.onPostNewCustomer(this.dataCarCustomer, data, this.dataImg.ID, this.dataBrand, this.dataColor, phone)
        this.addCarCustomerForm.reset();
        this.onSuccessForm()
      }
    }
  }

  onSubmitAddNewCarOldCustomer() {
    if (this.addCarCustomerForm.invalid) {
      this.showSuccess = false;
      this.showWarning = true;
    } else {
      this.loading = true;
      if (this.dataImg == null) {
        this.dataImg = "../assets/images/upload/carCust.jpg"
        this.postDataService.onPostNewCarCustomer(this.dataCarCustomer, this.customer_ID, this.customers_name, this.dataImg, this.dataBrand, this.dataColor)
        this.addCarCustomerForm.reset();
        this.onSuccessForm()
      } else {
        this.postDataService.onPostNewCarCustomer(this.dataCarCustomer, this.customer_ID, this.customers_name, this.dataImg.ID, this.dataBrand, this.dataColor)
        this.addCarCustomerForm.reset();
        this.onSuccessForm()
      }
    }
  }

  onItemSelectBrand(item: any) {
    this.brand_ID = item.id;
  }

  onItemSelectColor(item: any) {
    this.color_ID = item.id;
  }

  onItemSelectCustomer(item: any) {
    this.customer_ID = item.ID;
    this.customers_name = item.custName;
  }

  onItemDeSelectBrand() {
    this.brand_ID = null;
  }

  onItemDeSelectColor() {
    this.color_ID = null;
  }

  onItemDeSelectCustomer() {
    this.customer_ID = null;
  }

  onOpenAddNewCustomer(inAddNewCustomer) {
    this._modalService.open(inAddNewCustomer)
  }

  onOpenAddNewCarOldCustomer(inAddNewCarOldCustomer) {
    this._modalService.open(inAddNewCarOldCustomer)
  }

  onGetCustomer() {
    var cust = []
    this.getDataService.onGetCustomers()
      // .then(res => {console.log(res);})
      .then((res) => {
        for (let i = 0; i < res.data.length; i++) {
          for (let i2 = 0; i2 < res.data[i].customer.phones.length; i2++) {
            var defaultx = res.data[i].customer.phones[i2].default
            var numberPhone = res.data[i].customer.phones[i2].phone
          }
          for (let i3 = 0; i3 < res.data[i].cars.length; i3++) {
            var licence = res.data[i].cars[i3].plate_number
          }
          if (defaultx == true) {
            cust.push({
              ID: res.data[i].customer.ID,
              dataCustomer: res.data[i].customer.customer_name + " ทะเบียนรถ : " + licence + " เบอร์ : " + numberPhone,
              custName: res.data[i].customer.customer_name
            })
          }
        }
        this.customers = cust;
      })
  }

  onSuccessForm() {
    this.submitted = false;
    this.loading = false;
    this.showDanger = false;
    this.showWarning = false;
    this.showSuccess = true;
    this.addCarCustomerForm.reset();
  }

  onResetForm() {
    this.submitted = false;
    this.loading = false;
    this.showDanger = false;
    this.showWarning = false;
    this.showSuccess = false;
    this.addCarCustomerForm.reset();
  }

}
