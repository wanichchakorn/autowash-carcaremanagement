import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { GetdataService } from '../../_services/getdata.service'
import { PostdataService } from 'src/app/_services/postdata.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css']
})
export class AddJobComponent implements OnInit {

  addJobForm: FormGroup;

  disabled = false;
  item: any;
  selectedItem: any = [];
  dropdownCarCustomer: any = {};
  dropdownService: any = {};
  closeDropdownSelection = false;
  submitted = false;
  loading = false;
  returnUrl: string;

  car_ID: string = null;
  service_ID: any = [];
  service: any = [];
  service_name: string;
  carCustomers: any = [];

  showSuccess: boolean;
  showWarning: boolean;
  showDanger: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private getDataService: GetdataService,
    private postDataService: PostdataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  async ngOnInit() {
    this.addJobForm = this.formBuilder.group({
      incarCustomers: [null, [Validators.required]],
      inservice: [null, [Validators.required]]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.showSuccess = false;
    this.showWarning = false;
    this.showDanger = false;

    this.dropdownCarCustomer = {
      singleSelection: true,
      searchPlaceholderText: 'ค้นหา',
      noDataAvailablePlaceholderText: 'ไม่พบข้อมูล',
      allowSearchFilter: true,
      idField: 'ID',
      textField: 'car',
      closeDropDownOnSelection: this.closeDropdownSelection
    };

    this.dropdownService = {
      singleSelection: false,
      searchPlaceholderText: 'ค้นหา',
      noDataAvailablePlaceholderText: 'ไม่พบข้อมูล',
      allowSearchFilter: true,
      enableCheckAll: false,
      idField: 'ID',
      textField: 'service_name',
      itemsShowLimit: 5,
    };

    this.service = await this.getDataService.onGetService();
    this.carCustomers = await this.getDataService.onGetCustomer();
  }
  get f() { return this.addJobForm.controls; }

  onItemSelectCar(item: any) {
    this.car_ID = item.ID;
  }

  onItemSelectService(item: any) {
    this.service_ID.push(item.ID)
  }

  onItemDeSelectCar() {
    this.car_ID = null;
  }

  onItemDeSelectService(item: any) {
    const index = this.service_ID.indexOf(item.ID);
    if (index > -1) {
      this.service_ID.splice(index, 1);
    }
  }

  async onSubmit() {
    this.submitted = true;
    if (this.addJobForm.invalid) {
      this.showSuccess = false;
      this.showWarning = true;
      this.showDanger = false;
    } else {
      this.loading = true;
      await this.postDataService.onPostCarCustomer(this.car_ID, this.service_ID)
        .then(result => {
          if (result.success == true) {
            this.showSuccess = true;
            this.showWarning = false;
            this.showDanger = false;
          }
          else if (result.success == false) {
            this.showDanger = true;
            this.showWarning = false;
            this.showSuccess = false;
          }
        })
    }
    this.onReset()
  }

  onReset() {
    this.submitted = false;
    this.loading = false;
    this.car_ID = null;
    this.service_ID = [];
    this.addJobForm.reset();
  }

  onResetForm() {
    this.submitted = false;
    this.loading = false;
    this.car_ID = null;
    this.service_ID = [];
    this.showDanger = false;
    this.showWarning = false;
    this.showSuccess = false;
    this.addJobForm.reset();
  }

  toggleCloseDropdownSelection() {
    this.closeDropdownSelection = !this.closeDropdownSelection;
    this.dropdownService = Object.assign({}, this.dropdownService, 
    { closeDropDownOnSelection: this.closeDropdownSelection });
  }

  goToCarCustomer(){
    this.router.navigateByUrl('/staff/addcarcustomer');
  }

}

