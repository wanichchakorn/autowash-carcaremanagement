import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule, NgbModalModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { StaffRoutes } from './staff.routing';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DragulaModule } from 'ng2-dragula';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxPrintModule } from 'ngx-print';

import { HomeComponent } from "./home/home.component";
import { QueuedCustomerComponent } from './queued-customer/queued-customer.component';
import { ChangeBranchComponent } from './change-branch/change-branch.component';
import { AddJobComponent } from './add-job/add-job.component';
import { ServiceComponent } from './service/service.component';
import { FollowUpComponent } from './followup/followup.component';
import { CarCustomerComponent } from './car-customer/car-customer.component';
import { PostdataService } from '../_services/postdata.service';
import { GetdataService } from '../_services/getdata.service';
import { ChangetypeService } from '../_services/changetype.service';
import { FormatDateService } from '../_services/format-date.service';
import { AddCarCustomerComponent } from './add-car-customer/add-car-customer.component';
import { PromotionComponent } from './promotion/promotion.component';
import { AboutAutowashComponent } from './about-autowash/about-autowash.component';
import { DownloadAutowashComponent } from './download-autowash/download-autowash.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CarInStoreComponent } from './car-in-store/car-in-store.component';
import { DashboardService } from '../_services/dashboard.service';
import { CustomerComponent } from './customer/customer.component';
import { NotifierModule } from 'angular-notifier';

@NgModule({
  declarations: [
    HomeComponent,
    QueuedCustomerComponent,
    ChangeBranchComponent,
    AddJobComponent,
    ServiceComponent,
    FollowUpComponent,
    CarCustomerComponent,
    AddCarCustomerComponent,
    PromotionComponent,
    AboutAutowashComponent,
    DownloadAutowashComponent,
    ProfileComponent,
    DashboardComponent,
    CarInStoreComponent,
    CustomerComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    NgbModule,
    NgxPrintModule,
    RouterModule.forChild(StaffRoutes),
    DragulaModule.forRoot(),
    NgbModalModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    PerfectScrollbarModule,
    Ng2SmartTableModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    MatDialogModule,
    NotifierModule
  ],
  entryComponents: [
    QueuedCustomerComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
      PostdataService,
      GetdataService,
      DashboardService,
      ChangetypeService,
      FormatDateService,
      NgbActiveModal
  ]
})
export class StaffModule { }
