import { Component, OnInit, ViewChild } from '@angular/core';
import { GetdataService } from 'src/app/_services/getdata.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostdataService } from 'src/app/_services/postdata.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  newData: any = [];
  rows: any = [];
  temp = [...this.newData];
  loadingIndicator = true;
  reorderable = true;

  updateCustomerForm: FormGroup;

  @ViewChild(CustomerComponent, { static: false }) table: CustomerComponent;
  custName: string;
  custPhone: string;
  custId: string;
  imgUser: any;
  message: string = null;
  imgURL: any  = null;
  imagePath: any = null;
  dataImg: any = null;
  showSuccess = false;
  showWarning = false;
  showDanger = false;
  oldImgUser: any = null;

  constructor(
    private getDataService: GetdataService,
    private postDataService: PostdataService,
    private formBuilder: FormBuilder,
    private _modalService: NgbModal
    ) { }

  ngOnInit(): void {
    this.onFollowUp();
    setTimeout(() => {
      this.loadingIndicator = false;
    }, 1500);

    this.updateCustomerForm = this.formBuilder.group({
      inImg: [null],
      inNameCustomer: [null, [Validators.required]],
      inNumberPhone: [null, [Validators.required]]
    });
  }

  onFollowUp() {
    this.getDataService.onGetCustomers()
      .then((res)=>{ 
        for (var i = 0; i < res.data.length; i++) {
          for (var i2 = 0; i2 < res.data[i].customer.phones.length; i2++) {
            var phonesx = res.data[i].customer.phones[i2].phone
            var defaultx = res.data[i].customer.phones[i2].default
          }
          if (defaultx == true) {
            let cars = res.data[i].cars
            this.newData.push({
              customerId: res.data[i].customer.ID,
              customerName: res.data[i].customer.customer_name,
              phone: phonesx,
              brandName: cars[0].brand_name,
              model: cars[0].model,
              plateNumber: cars[0].plate_number
            })
          }
        }
        this.rows = this.newData;
        this.temp = [...this.newData];
      });
  }

  updateFilter(event) {
    // console.log(event.target.value)
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      // console.log(d.customerName)
      return d.customerName.toLowerCase().indexOf(val) !== -1 || !val ||
        d.brandName.toLowerCase().indexOf(val) !== -1 || !val ||
        d.model.toLowerCase().indexOf(val) !== -1 || !val ||
        d.plateNumber.toLowerCase().indexOf(val) !== -1 || !val ||
        d.phone.toLowerCase().indexOf(val) !== -1 || !val ;
    });
    this.rows = temp;
    this.table = this.newData;
  }

  imageUser(fileInput) {
    // console.log(fileInput);
    this.imgUser = fileInput.files[0]
    // console.log(fileInput.files[0]);
    if (this.imgUser == null) {
      this.message = "อัพโหลดรูป";
      return;
    }else{
      this.message =  this.imgUser.name
    }
    var reader = new FileReader();
    this.imagePath = fileInput.files[0];
    reader.readAsDataURL(this.imgUser); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
    this.onCheckImg()
  }

  async onCheckImg() {
      if (this.imgUser == null) {
        this.imgUser = null;
      } else {
        await this.postDataService.onUploadImg(this.imgUser)
          .then(res => { 
            this.dataImg = res.ID 
            return this.dataImg;
          })
        // .then(res => {console.log(this.dataImg)})
      }
  }

  async onEditCust(customer,inEditCustomer) {
    this.custId = customer.customerId
    this.custName = customer.customerName
    this.custPhone = customer.phone
    let custData = await this.getDataService.onGetCustomerProfile(this.custId)
    this.oldImgUser = custData.data[0].customer
    // console.log(this.oldImgUser);
    if(this.oldImgUser.profile_image == undefined) {
      this.imgURL = "../assets/images/users/user.png"
      this.oldImgUser = null;
    }else{
      this.imgURL = this.oldImgUser.profile_image.image_path
    }
    this.updateCustomerForm.setValue({
      inImg: "",
      inNameCustomer: this.custName,
      inNumberPhone: this.custPhone
    })
    // console.log(this.custName,this.custPhone);
    this._modalService.open(inEditCustomer)
  }

  onSubmitUpdateCustomer() {
    console.log(this.dataImg);
    if (this.updateCustomerForm.invalid) {
      this.showSuccess = false;
      this.showWarning = true;
    } else {
      if (this.dataImg == null) {
        this.dataImg = this.oldImgUser.profile_image_id
        this.postDataService.onPostUpdateCustomer(this.custId, this.updateCustomerForm.value, this.dataImg)
        this.updateCustomerForm.reset();
        location.reload();
        // console.log('1',this.dataImg)
      } else {
        this.postDataService.onPostUpdateCustomer(this.custId, this.updateCustomerForm.value, this.dataImg)
        this.updateCustomerForm.reset();
        location.reload();
        // console.log('2',this.dataImg)
      }
    }
  }

}
