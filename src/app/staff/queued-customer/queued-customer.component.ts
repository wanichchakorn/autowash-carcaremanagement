import { Component, OnInit } from '@angular/core';
import { GetdataService } from '../../_services/getdata.service'
import { Subscription, interval } from 'rxjs';
import { DragulaService } from 'ng2-dragula';
import { ChangetypeService } from '../../_services/changetype.service';
import { FormatDateService } from '../../_services/format-date.service';

import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/_services/dashboard.service';
// import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-queued-customer',
  templateUrl: './queued-customer.component.html',
  styleUrls: ['./queued-customer.component.scss']
})

export class QueuedCustomerComponent implements OnInit {

  customerName: string;
  brandName: string;
  modelName: string;
  plateNumber: string;
  cusStatus: string;
  cusType: string;
  serviceName: string;

  taskgroup = 'taskgroup';
  subs = new Subscription();
  addPriceForm: FormGroup;

  listQueueJob = [];
  listStartedJob = [];
  listDoneJob = [];
  listStatusJob = [];
  carServices: any;
  total: number;
  prices: number;
  minus: number;
  promotion: number;
  billCar: any;
  date: any;
  pricePay: number;

  // private notifier: NotifierService;

  constructor(
    private getDataService: GetdataService,
    private dragulaService: DragulaService,
    private changeTypeService: ChangetypeService,
    private formatDateService: FormatDateService,
    private dashboardService: DashboardService,
    public modal: NgbActiveModal,
    private _modalService: NgbModal,
    private formBuilder: FormBuilder,
    // private notifier: NotifierService
  ) {
    this.subs.add(this.dragulaService.dropModel(this.taskgroup)
      .subscribe(({ item }) => {
        // console.log('dropModel:');
        this.onCheckStatus(item);
      })
    );
    // this.notifier = notifier;
  }


  ngOnInit() {
    this.addPriceForm = this.formBuilder.group({
      price: [null, [Validators.required]],
      promotion: [null]
    });
    this.onCheckJob();
    interval(3000 * 60).subscribe(() => {
      this.onCheckJob();
    });
    this.onDate();
  }

  onCheckJob() {
    this.listQueueJob = [];
    this.listStartedJob = [];
    this.listDoneJob = [];
    this.listStatusJob = [];
    this.getDataService.onGetJob()
      .then(res => {
        // console.log(res)
        for (var i = 0; i < res.data.length; i++) {
          this.cusType = res.data[i].type
          if (this.cusType == '1-QUEUE') {
            this.listQueueJob.push(res.data[i]);
          } else if (this.cusType == '2-STARTED') {
            this.listStartedJob.push(res.data[i]);
            for (var i2 = 0; i2 < res.data[i].car_services.length; i2++) {
              this.cusStatus = res.data[i].car_services[i2].status
              // if (this.cusStatus == "P") {
              // } else if (this.cusStatus == "I") {
              // } else if (this.cusStatus == "C") {
              // }
            }
          } else if (this.cusType == '3-DONE') {
            this.listDoneJob.push(res.data[i]);
          }
        }
      });
  }

  onCheckStatus(item) {
    // console.log(item)
    const items = item;
    if (items.type == '1-QUEUE') {
      this.changeTypeService.onStartJob(items.ID)
    }
  }

  onDone(item) {
    this.changeTypeService.onDoneJob(item.ID);
    setTimeout(() => {
      this.onCheckJob();
    }, 1000);
  }

  onDelete(item, inDelete) {
    this._modalService.open(inDelete)
    this.changeTypeService.onGetDelete(item.ID)
  }

  onPay(item, inPrice) {
    // console.log(item)
    this._modalService.open(inPrice)
    this.total = 0;
    this.changeTypeService.onGetPay(item.ID);
    this.carServices = item.car_services
    for (var i = 0; i < this.carServices.length; i++) {
      this.prices = this.carServices[i].price;
      this.total  = this.total  + this.prices;
    }
    this.addPriceForm.setValue({
      price: this.total,
      promotion: ''
    })
  }

  onBill(item, inBill) {
    this._modalService.open(inBill)
    this.carServices = item.car_services
    this.pricePay = item.price_decimal
    this.billCar = item;
    // console.log(this.billCar)
    this.total = 0;
    for (var i = 0; i < this.carServices.length; i++) {
      this.prices = this.carServices[i].price;
      this.total = this.total + this.prices;
      this.promotion = this.total - this.pricePay;
    }
  }

  onCheckPay(data) {
    // console.log(data)
    if (data.status == "PAY") {
      return true;
    } else {
      return false;
    }
  }

  onJobStatus(data) {
    // console.log(data)
    if (data.status == "C") {
      return '1';
    } else if (data.status == "I") {
      return '2';
    } else {
      return '0';
    }
  }

  onPayStatus(data) {
    // console.log(data)
    if (data.status == "PAID") {
      return true;
    } else {
      return false;
    }
  }

  onKey(event: any,total: number) {
    // console.log(total) 
    this.promotion = event.target.value ;
    this.minus = total - event.target.value ;
    this.addPriceForm.setValue({
      price: this.minus,
      promotion: this.promotion
    })
  }

  onSubmitPay(data) {
    // console.log(data)
    this.changeTypeService.onGetPrice(data)
    setTimeout(() => {
      this.onCheckJob();
    }, 1000);
  }

  onSubmitDel() {
    const confirm = true;
    this.changeTypeService.onDeleteConfirm(confirm);
    setTimeout(() => {
      this.onCheckJob();
    }, 1000);
  }

  onDate() {
    this.date = this.formatDateService.config()
    // console.log(this.date)
  }

}
