import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  firstName: string;
  mobile: string;
  dateBirth: string;
  branchName: string;
  imageBranch: string;
  lastName: string;

  constructor(
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this.firstName = this.cookieService.get('first_name');
    this.lastName = this.cookieService.get('last_name');
    this.mobile = this.cookieService.get('mobile_no');
    this.dateBirth = this.cookieService.get('date_of_birth');
    this.branchName = this.cookieService.get('branch_name');
    this.imageBranch = this.cookieService.get('image_path');
  }

}
