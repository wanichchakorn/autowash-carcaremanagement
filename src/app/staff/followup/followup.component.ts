import { Component, OnInit, ViewChild } from '@angular/core';
import { GetdataService } from 'src/app/_services/getdata.service';

@Component({
  selector: 'app-follow',
  templateUrl: './followup.component.html',
  styleUrls: ['./followup.component.css']
})

export class FollowUpComponent implements OnInit {

  newData: any = [];
  rows: any = [];
  cust: any = [];
  brandName: any;
  model: any;
  plateNumber: any;
  customerName: any;
  temp = [...this.newData];
  loadingIndicator = true;
  reorderable = true;

  // columns = [{ name: 'ชื่อลูกค้า' }, { name: 'เบอร์โทรศัพท์' }, { name: 'รถลูกค้า' }, { name: 'ทะเบียนรถลูกค้า' }];

  @ViewChild(FollowUpComponent, { static: false }) table: FollowUpComponent;

  constructor(private getDataService: GetdataService) { }

  ngOnInit(): void {
    this.onFollowUp();
    setTimeout(() => {
      this.loadingIndicator = false;
    }, 1500);
  }

  onFollowUp() {
    this.getDataService.onGetFollowUp()
      .then((res) => {

        for (var i = 0; i < res.data.length; i++) {
          for (var i2 = 0; i2 < res.data[i].cars.length; i2++) {
            this.brandName = res.data[i].cars[i2].brand_name
            this.model = res.data[i].cars[i2].model
            this.plateNumber = res.data[i].cars[i2].plate_number
            this.cust = res.data[i].cars[i2].customer

            for (var i3 = 0; i3 < this.cust.phones.length; i3++) {
              var phonesx = this.cust.phones[i3].phone
              var defaultx = this.cust.phones[i3].default

              if (defaultx == true) {
                this.newData.push({
                  customer_name: this.cust.customer_name,
                  phone: phonesx,
                  brandName: this.brandName,
                  model: this.model,
                  plateNumber: this.plateNumber
                })
              }
            }
          }
        }
        this.rows = this.newData;
        this.temp = [...this.newData];
      });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.customer_name.toLowerCase().indexOf(val) !== -1 || !val ||
        d.phone.toLowerCase().indexOf(val) !== -1 || !val ||
        d.brandName.toLowerCase().indexOf(val) !== -1 || !val ||
        d.model.toLowerCase().indexOf(val) !== -1 || !val ||
        d.plateNumber.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table = this.newData;
  }

}
