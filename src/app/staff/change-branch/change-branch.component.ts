import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-change-branch',
  templateUrl: './change-branch.component.html',
  styleUrls: ['./change-branch.component.css']
})
export class ChangeBranchComponent implements OnInit {

  public edited = false;
  listbranch = [];
  branch: string;
  branch0: number;
  getBranch: any;

  constructor(
    private router: Router,
    private cookieService: CookieService) { }

  onGetJob() {
    var myHeaders = new Headers();
    const authen: string = this.cookieService.get('authorization');
    // console.log(authen)
    myHeaders.append('authorization', authen);
    var requestOptions: RequestInit = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch(`${environment.API_URL}/api/v1/auth/branch/branches`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        // console.log(res)
        this.onCheckbranch(res);
      })
      .catch(error => console.log('error', error));
  }

  ngOnInit() {
    this.onGetJob();
  }

  onCheckbranch(res) {
    // console.log(res)
    for (var i = 0; i < res.data.length; i++) {
      this.getBranch = res.data[i].ID
      this.branch = res.data[i].ID[i].length
      this.listbranch.push(res.data[i]);

        if(this.branch > '1'){
          this.listbranch.push(res.data[i]);
        }else{
          this.cookieService.set('branch_id', this.getBranch);
          this.router.navigateByUrl('staff/queued');
          // console.log(this.cookieService.get('branch_id'));
        }
    }
  }

  onChangeBranch(item) {
    this.cookieService.set('branch_id', item.ID);
    this.router.navigateByUrl('staff/queued');
    // console.log(this.cookieService.get('branch_id'));
  }

}
