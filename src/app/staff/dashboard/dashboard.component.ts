import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DashboardService } from 'src/app/_services/dashboard.service';
import * as c3 from 'c3';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  newDate = new Date();
  format = 'yyyyMMdd';
  locale = 'en-US';
  payCount: number;
  carCount: number;
  serviceCount: number;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.onToDay()
  }

  onDashBoard(count) {
    const chart = c3.generate({
      bindto: '#visitor',
      data: {
        columns: [
          ['จำนวนรถที่เข้าใช้บริการ', 5],
          ['จำนวนบริการที่เข้าใช้', count || null]
        ],
        type: 'donut'
      },
      donut: {
        label: {
          show: false
        },
        title: 'Ratio',
        width: 50
      },
      legend: {
        hide: true
      },
      color: {
        pattern: ['#40c4ff', '#2961ff', '#ff821c', '#7e74fb']
      }
    });
  }

  onToDay() {
    this.onGetDashboard(this.newDate)
  }

  onLastWeek() {
    const date = new Date();
    const pastDate = this.newDate.getDate() - 7;
    date.setDate(pastDate);
    this.onGetDashboard(date)
  }

  onLastMonth() {
    const date = new Date();
    const previousMonth = new Date(date);
    previousMonth.setMonth(date.getMonth() - 1);
    this.onGetDashboard(previousMonth)
  }

  onLastYear() {
    const date = new Date();
    const previousYear = new Date(date);
    previousYear.setUTCFullYear(date.getFullYear() - 1);
    this.onGetDashboard(previousYear)
  }

  onGetDashboard(data) {
    const startDate = formatDate(data, this.format, this.locale);
    const endDate = formatDate(this.newDate, this.format, this.locale);
    this.dashboardService.onGetCount(startDate, endDate)
      .then(res => {
        // console.log('จำนวนการใช้บริการ',res)
        this.serviceCount = 0
        for (let i = 0; i < res.data.length; i++) {
          if (res.data[i].count >= 1) {
            this.serviceCount += res.data[i].count
          }
        }
        this.onDashBoard(this.serviceCount)
      })
    this.dashboardService.onGetTransactionCount(startDate, endDate)
    .then(res => {
      // console.log('จำนวนรถ',res)
      this.carCount = 0
      this.carCount = res.data.total
      this.onDashBoard(this.carCount)
    })
    this.dashboardService.onGetTransactionPayment(startDate, endDate)
    .then(res => {
      // console.log('ค่าบริการ',res)
      this.payCount = 0
      this.payCount = res.data.total
      this.onDashBoard(this.payCount)
    })
  }

}
