import { Component, OnInit, ViewChild } from '@angular/core';
import { GetdataService } from 'src/app/_services/getdata.service';

@Component({
  selector: 'app-car-customer',
  templateUrl: './car-customer.component.html',
  styleUrls: ['./car-customer.component.css']
})
export class CarCustomerComponent implements OnInit {

  newData: any = [];
  rows: any = [];
  cust: any = [];
  brand: any;
  model: any;
  plateNumber: any;
  temp = [...this.newData];
  loadingIndicator = true;
  reorderable = true;

  @ViewChild(CarCustomerComponent, { static: false }) table: CarCustomerComponent;

  constructor(private getDataService: GetdataService) { }

  ngOnInit(): void {
    this.onFollowUp();
    setTimeout(() => {
      this.loadingIndicator = false;
    }, 1500);
  }

  onFollowUp() {
    this.getDataService.onGetCarCustomer()
      .then((res) => {

        for (var i = 0; i < res.data.length; i++) {
          for (var i2 = 0; i2 < res.data[i].cars.length; i2++) {
            this.brand = res.data[i].cars[i2].brand_name
            this.model = res.data[i].cars[i2].model
            this.plateNumber = res.data[i].cars[i2].plate_number
            this.cust = res.data[i].cars[i2].customer

            for (var i3 = 0; i3 < this.cust.phones.length; i3++) {

              this.newData.push({
                brand_name: this.brand,
                model: this.model,
                plateNumber: this.plateNumber,
                customer_name: this.cust.customer_name,
                phones: this.cust.phones[i3].phone
              })
            }
          }
        }
        this.rows = this.newData;
        this.temp = [...this.newData];
        // console.log(this.newData);
      });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.brand_name.toLowerCase().indexOf(val) !== -1 || !val ||
        d.model.toLowerCase().indexOf(val) !== -1 || !val ||
        d.plateNumber.toLowerCase().indexOf(val) !== -1 || !val ||
        d.customer_name.toLowerCase().indexOf(val) !== -1 || !val ||
        d.phones.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table = this.newData;
  }
}
