import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUserForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  userLogin: boolean = false;
  user: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private cookieService: CookieService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginUserForm = this.formBuilder.group({
      phoneNumber: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(12)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(12)]]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  get f() { return this.loginUserForm.controls; }

  async onSubmit() {
    // console.log("submit")
    this.submitted = true;
    if (this.loginUserForm.invalid) {
      return;
    }
    this.loading = true;
    await this.authenticationService.onLogin(this.f.phoneNumber.value, this.f.password.value)
    this.onCheckUserLogin();
  }

   onCheckUserLogin() {
    // console.log(this.f.phoneNumber.value)
    this.user = this.cookieService.get('mobile_no')
    // console.log(this.user)
    if (this.f.phoneNumber.value == this.user) {
      this.userLogin = true;
    } else {
      this.userLogin = false;
      var popup = document.getElementById("alertPopup");
      popup.classList.toggle("show");
      setTimeout(location.reload.bind(location), 4000);
      return;
    }
  }

  goToRegister(){
    // console.log('register')
    this.router.navigateByUrl('/register');
  }

}
