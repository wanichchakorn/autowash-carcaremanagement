import { Component, OnInit } from '@angular/core';
import { ROUTES } from './menu-items';
import { RouteInfo } from './sidebar.metadata';
import { CookieService } from 'ngx-cookie-service';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems: any = [];

  data: RouteInfo[];
  status: any;

  // this is for the open close
  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }
  addActiveClass(element: any) {
    if (element === this.showSubMenu) {
      this.showSubMenu = '0';
    } else {
      this.showSubMenu = element;
    }
  }

  constructor(
    private cookieService: CookieService
  ) { }

  // End open close
  ngOnInit() {
    this.onShowMenu();
  }

  onShowMenu() {
    this.data = ROUTES.filter(res => res);
    for (var i = 0; i < this.data.length; i++) {
      // console.log(this.data.length);
      
      if (this.cookieService.get('role_id') == "ROLE_000" && this.data[i].status == "allUser") {

        if (this.data[i].status == "allUser") {
          this.status = this.data[i].status
          // console.log(this.status);
        }

        this.sidebarnavItems.push({
          path: this.data[i].path,
          title: this.data[i].title,
          icon: this.data[i].icon,
          class: this.data[i].class,
          ddclass: this.data[i].ddclass,
          status: this.status,
          extralink: this.data[i].extralink,
          submenu: this.data[i].submenu
        })
      }else if(this.cookieService.get('role_id') == "ROLE_001") {
        this.sidebarnavItems = this.data
      }
    }
  }

}
