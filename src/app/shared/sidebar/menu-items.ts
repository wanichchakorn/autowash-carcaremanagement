import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '',
    title: 'ระบบการจัดการงาน',
    icon: 'mdi mdi-library-books',
    class: 'has-arrow',
    ddclass: '',
    status: "allUser",
    extralink: false,
    submenu: [
      {
        path: '/staff/queued',
        title: 'รายการดำเนินงาน',
        icon: 'mdi mdi-file-document-box',
        class: '',
        ddclass: '',
        status: "allUser",
        extralink: false,
        submenu: []
      },
      {
        path: '/staff/addjob',
        title: 'เพิ่มงาน',
        icon: 'mdi mdi-plus-box',
        class: '',
        ddclass: '',
        status: "allUser",
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: 'ระบบการจัดการรถลูกค้า',
    icon: 'mdi mdi-car',
    class: 'has-arrow',
    ddclass: '',
    status: "allUser",
    extralink: false,
    submenu: [
      {
        path: '/staff/carinstore',
        title: 'รายชื่อรถที่ใช้บริการอยู่',
        icon: 'mdi mdi-timelapse',
        class: '',
        ddclass: '',
        status: "allUser",
        extralink: false,
        submenu: []
      },
      {
        path: '/staff/carcustomer',
        title: 'ข้อมูลรถลูกค้า',
        icon: 'mdi mdi-car-connected',
        class: '',
        ddclass: '',
        status: "allUser",
        extralink: false,
        submenu: []
      },
      {
        path: '/staff/addcarcustomer',
        title: 'เพิ่มรถลูกค้า',
        icon: 'mdi mdi-plus-box',
        class: '',
        ddclass: '',
        status: "allUser",
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: 'ระบบตรวจสอบข้อมูลลูกค้า',
    icon: 'mdi mdi-database',
    class: 'has-arrow',
    ddclass: '',
    status: "allUser",
    extralink: false,
    submenu: [
      {
        path: '/staff/customer',
        title: 'รายชื่อลูกค้า',
        icon: 'mdi mdi-face',
        class: '',
        ddclass: '',
        status: "allUser",
        extralink: false,
        submenu: []
      },
      {
        path: '/staff/followup',
        title: 'ประวัติการเข้าใช้บริการ',
        icon: 'mdi mdi-clipboard-account',
        class: '',
        ddclass: '',
        status: "allUser",
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: 'ระบบการจัดการร้าน',
    icon: 'mdi mdi-cart',
    class: 'has-arrow',
    ddclass: '',
    status: "owner",
    extralink: false,
    submenu: [
      {
        path: '/staff/service',
        title: 'รายการบริการ',
        icon: 'mdi mdi-file-document-box',
        class: '',
        ddclass: '',
        status: "owner",
        extralink: false,
        submenu: []
      },
      {
        path: '/staff/dashboard',
        title: 'สรุปยอด',
        icon: 'mdi mdi-chart-pie',
        class: '',
        ddclass: '',
        status: "owner",
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: '',
    icon: '',
    class: '',
    ddclass: '',
    status: "allUser",
    extralink: true,
    submenu: []
  }
];
