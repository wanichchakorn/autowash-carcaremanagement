import { Component, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { CookieService } from 'ngx-cookie-service';

import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services/authentication.service';

declare var $: any;

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent implements AfterViewInit {
  @Output() toggleSidebar = new EventEmitter<void>();

  public config: PerfectScrollbarConfigInterface = {};

  public showSearch = false;
  firstName: string;
  mobile: string;

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngAfterViewInit() {

  }

  ngOnInit() {
    this.firstName = this.cookieService.get('first_name');
    this.mobile = this.cookieService.get('mobile_no');
  }

  toProfile() {
    this.router.navigate(['staff/profile']);
  }
  toAbout() {
    this.router.navigate(['staff/aboutautowash']);
  }
  toDownload() {
    this.router.navigate(['staff/downloadsutowash']);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
