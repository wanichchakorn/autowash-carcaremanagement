export class User {
    id: number;
    phoneNumber: string;
    password: string;
    token: string;
}