import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    role: string;
    constructor(
        private router: Router,
        private cookieService: CookieService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (this.cookieService.get('authorization') && this.cookieService.get('role_id') == "ROLE_000") {
            route.data.Role
            // console.log('staff');
            return true;
        } else if (this.cookieService.get('authorization') && this.cookieService.get('role_id') == "ROLE_001") {
            route.data.Role
            // console.log('owner');
            return true;
        } else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        }
    }

}