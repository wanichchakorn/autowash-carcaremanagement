import { Component, AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html'
})
export class NotfoundsComponent implements AfterViewInit {

  constructor(public router: Router){}
  ngAfterViewInit() {}
  onGoToLogin(){
    this.router.navigate(['/login'])
  }
  
}


